# Change Log

## [0.2] - 2018-04-21
### Added
- Light turns off on boot
- Web application

## [0.1] - 2018-04-21
### Added
- Initial Release
