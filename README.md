# Litaf

Lit AF integrates Twilio, Hue with some Kent Magic

## Compile and run the source

Remember to set the GOPATH variable.

```
cd ${GOPATH}/src
git clone https://gitlab.com/iarenzana/litaf
cd litaf
go get -u
make
```

## Controller Usage

```
litaf -user [hue_developer_id] -light 1 -bridge [bridge_ip] -twilioaccount [twilio_user_account] - twiliotoken [twilio_api_token]
```

## Install ngrok and Run ngrok

Go to [ngrok.io](https://ngrok.io) and download the app.

```
ngrok authtoken [authentication_token]
ngrok http 12000 --subdomain litaf
```

## Web UI
Browse to File URL in web browser:
```
file:///[litaf-folder]/web/index.html
```

## Steps for demo
1. Turn up network and light.
2. Start ngrok tunnel.
3. Start `litaf` controller with the proper bridge IP of the network.
4. Browse to the File URL in web browser
5. Send SMS to (317) 743-2226
6. Respond to SMS in the web ui (optional)
7. Click "TURN ON RESPONSE LIGHT" button on map popup.

## About

Crafted with :heart: in Indiana by The Stallions
