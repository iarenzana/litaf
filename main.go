package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"gitlab.com/iarenzana/litaf/objects"
)

const version = "0.1"

var TwilioToken string
var TwilioAccount string
var UserID string
var LightID string
var BridgeIP string

//StatusVar keeps track of a single status channel
var StatusVar objects.TwilioCallback

//Channel for statuses
// var StatusChannel chan objects.TwilioCallback

func init() {

}
func main() {

	// StatusChannel = make(chan objects.TwilioCallback)
	var srv http.Server
	//Capture signals to correctly close channels and connections
	sigc := make(chan os.Signal, 1)

	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	go func() {
		s := <-sigc
		switch s {
		case os.Interrupt:
			log.Printf("Closing connections...\n")
			srv.Close()
			os.Exit(0)
		case syscall.SIGTERM:
			log.Printf("Closing connections...\n")
			srv.Close()
			os.Exit(-1)
		}
	}()

	userID := flag.String("user", "", "Hue User")
	lightID := flag.String("light", "", "Hue Light ID")
	bridgeIP := flag.String("bridge", "", "Bridge IP")
	twilioAccount := flag.String("twilioaccount", "", "Twilio Account Key")
	twilioToken := flag.String("twiliotoken", "", "Twilio API Key")

	flag.Parse()
	UserID = *userID
	LightID = *lightID
	BridgeIP = *bridgeIP
	TwilioAccount = *twilioAccount
	TwilioToken = *twilioToken

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/get-calls", reportStatus)
	router.HandleFunc("/flicker", flickerLights)
	router.HandleFunc("/send-message", echoText).Methods("POST")
	router.HandleFunc("/trigger", processIncomingPayload)

	log.Printf("Starting up litaf %v...\n", version)

	err := triggerLightOff()
	if err != nil {
		log.Printf("%v", err)
	}

	//Start server on the https port

	srv.Addr = fmt.Sprintf(":%v", 12000)
	srv.Handler = router

	if err = srv.ListenAndServe(); err != nil {
		log.Printf("Listening for requests on: %s\n", err)
	}

}

func processIncomingPayload(w http.ResponseWriter, r *http.Request) {
	log.Printf("Triggered process Incoming payload from %v", r.Header.Get("X-Forwarded-For"))

	err := r.ParseForm()
	if err != nil {
		log.Print("Error parsing form")
	}

	var twilioStuff objects.TwilioCallback
	decoder := schema.NewDecoder()

	err = decoder.Decode(&twilioStuff, r.Form)
	if err != nil {
		// log.Print("Error decoding JSON")
	}
	log.Printf("Received %s from %s\n", twilioStuff.Body, twilioStuff.From)

	StatusVar.Body = twilioStuff.Body
	StatusVar.From = twilioStuff.From

	log.Printf("%v    %v", twilioStuff.Body, twilioStuff.From)
}

func flickerLights(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	log.Printf("Triggered light flicker from %v", r.Header.Get("X-Forwarded-For"))

	err := triggerLightOn()
	if err != nil {
		log.Fatal("Error turning light on!")
	}

	err = triggerLightAlert()
	if err != nil {
		log.Fatal("Error triggering light!")
	}

	w.WriteHeader(http.StatusOK)
}

func triggerLightAlert() error {
	var hueURL = fmt.Sprintf("http://%v/api/%s/lights/%s/state", BridgeIP, UserID, LightID)
	var payloadBody = []byte(`{"alert":"lselect"}`)

	req, err := http.NewRequest("PUT", hueURL, bytes.NewBuffer(payloadBody))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("Error sending request!")
	}

	defer resp.Body.Close()

	if resp.StatusCode == 201 {
		return fmt.Errorf("Light must be on")
	} else if resp.StatusCode == 201 {
		return fmt.Errorf("Not found!")
	}

	return nil
}

func triggerLightOff() error {
	var hueURL = fmt.Sprintf("http://%v/api/%s/lights/%s/state", BridgeIP, UserID, LightID)
	var payloadBody = []byte(`{"on":false}`)

	req, err := http.NewRequest("PUT", hueURL, bytes.NewBuffer(payloadBody))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("%v\n", err)
		return fmt.Errorf("Error requesting light status")
	}

	defer resp.Body.Close()

	if resp.StatusCode == 201 {
		return fmt.Errorf("Light must be on")
	} else if resp.StatusCode == 201 {
		return fmt.Errorf("Not found!")
	}

	return nil
}

func triggerLightOn() error {
	var hueURL = fmt.Sprintf("http://%v/api/%s/lights/%s/state", BridgeIP, UserID, LightID)
	var payloadBody = []byte(`{"on":"true"}`)

	req, err := http.NewRequest("PUT", hueURL, bytes.NewBuffer(payloadBody))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return fmt.Errorf("Error sending request!")
	}

	defer resp.Body.Close()

	if resp.StatusCode == 201 {
		log.Fatal("Light must be on")
	} else if resp.StatusCode == 201 {
		log.Fatal("Not found!")
	}

	return nil
}

func echoText(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	err := r.ParseForm()
	if err != nil {
		log.Print("Error parsing form")
	}

	var tText objects.TwilioText
	decoder := schema.NewDecoder()

	err = decoder.Decode(&tText, r.Form)
	if err != nil {
		// log.Print("Error decoding JSON")
	}

	//Back to twilio
	// decoder := json.NewDecoder(r.Body)

	// var tText objects.TwilioText
	// err := decoder.Decode(&tText)
	// if err != nil {
	// 	log.Print("Error decoding Body")
	// }
	accountSid := TwilioAccount
	authToken := TwilioToken
	urlStr := "https://api.twilio.com/2010-04-01/Accounts/" + accountSid + "/Messages.json"
	v := url.Values{}
	v.Set("To", tText.To)
	v.Set("From", "+13177432226")
	v.Set("Body", tText.Body)
	rb := *strings.NewReader(v.Encode())

	client := &http.Client{}

	req, _ := http.NewRequest("POST", urlStr, &rb)
	req.SetBasicAuth(accountSid, authToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, _ := client.Do(req)
	log.Print(resp.Status)
}

func reportStatus(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	log.Printf("Triggered status report from %v", r.Header.Get("X-Forwarded-For"))

	// if len(StatusChannel) == 0 {
	// 	w.WriteHeader(http.StatusNoContent)
	// 	return
	// }

	// StatusVar := <-StatusChannel

	if StatusVar.From == "" {
		log.Print("No data in the queue")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	log.Print("Reporting data for " + StatusVar.From)

	w.WriteHeader(http.StatusOK)
	resp, err := json.Marshal(StatusVar)
	if err != nil {
		log.Print("Error Marshalling response!")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	StatusVar.From = ""
	StatusVar.Body = ""
	w.Write(resp)
	return
}
