package objects

//TwilioCallback represents data from Twilio
type TwilioCallback struct {
	From string
	Body string
}

//TwilioText Message back to Twilio
type TwilioText struct {
	To   string `json:"to"`
	Body string `json:"body"`
}
